/**
 * A dictionary of environments
 */
const environments = {
  DEVELOPMENT: "DEVELOPMENT",
  TEST:        "TEST",
  STAGING:     "STAGING",
  PRODUCTION:  "PRODUCTION"
}


/**
 * Get current environment
 */
const getEnvironment = () => {
  if (!Object.prototype.hasOwnProperty.call(process.env, "NODE_ENV"))
    return environments.DEVELOPMENT

  switch (process.env.NODE_ENV.toUpperCase()) {
  case "TEST":
    return environments.TEST
  case "PROD":
  case "PRODUCTION":
    return environments.PRODUCTION
  case "STAGING":
    return environments.STAGING
  case "DEV":
  case "DEVELOP":
  case "DEVELOPMENT":
  default:
    return environments.DEVELOPMENT
  }
}



Object.assign(global, {

  /**
   * Environments
   */
  environments,

  /**
   * Current Environment
   */
  environment: getEnvironment()

})
