import { scrypt, scryptSync, randomBytes } from "crypto"
import { promisify, format }               from "util"


const scryptAsync = promisify(scrypt)

const CHARS_DIGITS     = "1234567890"
const CHARS_ALPHABETS  = "abcdefghijklmnopqrstuvwxyz"
const CHARS_UALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const CHARS_SPECIALS   = "#!&@-=$%*+"


export default class Password {

  /**
   * Generate an encrypted password asynchronously
   *
   * @param {String} password - Plain text password
   * @param {Number} size - Number of bytes to generate salt, default is 1, recommonded at least 16 bytes long
   * @return {String} Encrypted generated password
   */
  static async EncryptAsync(password, size = 1) {
    if (password == null || typeof(password) !== "string")
      throw new TypeError("Password is invalid")
    if (!Number.isInteger(size) || size < 1)
      throw new TypeError("Size is invalid")

    const salt = randomBytes(size).toString("hex")
    const buf = await scryptAsync(password, salt, 64)

    return format("%s.%s", buf.toString("hex"), salt)
  }


  /**
   * Generate an encrypted password synchronously
   *
   * @param {String} password - Plain text password
   * @param {Number} size - Number of bytes to generate salt, default is 1, recommonded at least 16 bytes long
   * @return {String} Encrypted generated password
   */
  static EncryptSync(password, size = 1) {
    if (password == null || typeof(password) !== "string")
      throw new TypeError("Password is invalid")
    if (!Number.isInteger(size) || size < 1)
      throw new TypeError("Size is invalid")

    const salt = randomBytes(size).toString("hex")
    const buf = scryptSync(password, salt, 64).toString("hex")

    return format("%s.%s", buf, salt)
  }


  /**
   * Compare an encrypted password with plain password asynchronously
   * @param {String} encryptedPassword - An encrypted password
   * @param {String} plainPassword - An plain text password
   * @return {Boolean} True if they are equal
   */
  static async CompareAsync(encryptedPassword, plainPassword) {
    if (encryptedPassword == null || typeof(encryptedPassword !== "string"))
      throw new TypeError("Encrypted Password is invalid")

    const [hashedPassword, salt] = encryptedPassword.split(".")
    const buf = await scryptAsync(plainPassword, salt, 64)

    return buf.toString("hex") === hashedPassword
  }


  /**
   * Compare an encrypted password with plain password synchronously
   * @param {String} encryptedPassword - An encrypted password
   * @param {String} plainPassword - An plain text password
   * @return {Boolean} True if they are equal
   */
  static CompareSync(encryptedPassword, plainPassword) {
    if (encryptedPassword == null || typeof(encryptedPassword) !== "string")
      throw new TypeError("Encrypted password is invalid")

    const [hashedPassword, salt] = encryptedPassword.split(".")
    const buf = scryptSync(plainPassword, salt, 64).toString("hex")

    return buf === hashedPassword
  }


  /**
   * Generate One-Time Password
   *
   * @param {Number} length - Length of generated password
   * @return {String}
   */
  static OneTimePassword(length, { digits = true, alphabets = true, ualphabets = true, specials = true } = {}) {
    if (!Number.isInteger(length) || length < 1)
      throw new Error("Invalid length")

    let allowsChars = format("%s%s%s%s",
      (digits ? CHARS_DIGITS : ""),
      (alphabets ? CHARS_ALPHABETS : ""),
      (ualphabets ? CHARS_UALPHABETS : ""),
      (specials ? CHARS_SPECIALS : "")
    )

    let password = ""
    for (let index = 0; index < length; ++index)
      password += allowsChars[Math.floor(Math.random() * (allowsChars.length - 1))]

    return password
  }

}
