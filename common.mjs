/**
 * Counting a definite number, from 0 to target
 * If provide a callback function then callback will be called, else a generator will be returned
 *
 * @param {Number} target - target number to count
 * @param {Function|Null} callback - a function which contain a parameter to call
 * @return {Null|Generator} Return a generator if callback is not presented, else return Null
 */
export function Counting(target, callback) {
  if (!Number.isInteger(target))
    throw new TypeError("Invalid target")

  let idx = 0

  if (typeof(callback) === "function") {
    if (target > 0) {
      for (idx = 0; idx < target; idx++)
        callback(idx + 1)
    } else if (target < 0) {
      for (idx = 0; idx > target; idx--)
        callback(idx - 1)
    }
  }

  if (callback == null) {
    return function* generator() {
      if (target > 0) {
        for (idx = 0; idx < target; idx++)
          yield (idx + 1)
      } else if (target < 0) {
        for (idx = 0; idx > target; idx--)
          yield (idx - 1)
      }
    }
  }
}



/**
 * Generating a random number
 *
 * @param {Number} max - a non-zero maximum number
 * @param {Number} min - a non-negative minimum number
 * @return {Number} a random number
 */
export function Random(max, min = 0) {
  if (!Number.isInteger(max) || max < 1)
    throw new TypeError("Invalid max number")
  if (!Number.isInteger(min) || min < 0)
    throw new TypeError("Invalid min number")
  if (min > max)
    throw new TypeError("Max must be greather than min")

  return Math.floor(Math.random() * (max - min) + min)
}



/**
 * Bind class methods and functions to an instance
 *
 * @param {Object|Function} instance - an object instance
 * @param {Object|Null} owner - owner of this, if null owner become instance
 */
export function Binder(instance, owner) {
  if (instance == null)
    throw new TypeError("Invalid instance")
  owner = (owner != null && typeof(owner) === "object") ? owner : instance

  let target
  switch (typeof(instance)) {
  case "object":
    target = Object.getPrototypeOf(instance)
    break
  case "function":
    target = instance
    break
  default:
    throw new TypeError("Invalid instance")
  }

  Object.getOwnPropertyNames(target)
    .filter(name => instance[name] instanceof Function && name !== "constructor" && name !== "_")
    .forEach(name => { instance[name] = instance[name].bind(owner) })
}



/**
 * Check whether a value is empty or not
 *
 * @param {Object} value
 * @return {Boolean}
 */
export function IsEmpty(value) {
  if (typeof value === "number") return false
  else if (typeof value === "string") return value.trim().length === 0
  else if (Array.isArray(value)) return value.length === 0
  else if (typeof value === "object") return value == null || Object.keys(value).length === 0
  else if (typeof value === "boolean") return false
  else return !value
  // return value === (void 0)
}



/**
 * Check a data object contains desire properties
 *
 * @param {Object} data - Data object to check
 * @param {Array<String>} requirements - An array of requirements for checking
 * @return {Boolean}
*/
export function Contains(data, requirements) {
  if (data == null || typeof(data) !== "object")
    throw new TypeError("Invalid data")
  if (requirements == null || !Array.isArray(requirements))
    throw new TypeError("Invalid requirements")

  const items = Object.getOwnPropertyNames(data)
  return (requirements.every(field => items.includes(field)))
}



/**
 * String Formatter
 *
 * Example:
 *    Formatter("Hello, My name is {1} and my family is {2}, I was born in {age}", "Saleh", "Rahimzadeh", 1985)
 *
 * @param {String} message - The message template, which contains zero or more placeholders. Placeholders are numberic (start from 1) and named string (which replace by numberic position).
 * @param {Array<String>} args - Zero ro more arguments used to replace the corresponding placeholders in the message template.
 * @return {String} - The formatted message.
 */
export function Formatter(...args) {
  if (args.length === 0)
    return ""
  if (args.length === 1)
    return args[0]

  const [ message, ...parameters ] = args

  if (typeof(message) !== "string")
    throw new TypeError("Invalid message")
  if (message === "")
    throw new Error("Empty message")

  const PLACEHOLDER_REGEXP = /\{([0-9a-zA-Z]+)\}/g

  let index = -1

  return message.replace(PLACEHOLDER_REGEXP, (match, placeholder, offset) => {
    index++

    if (message[offset - 1] === "{" && message[offset + match.length] === "}")
      return placeholder

    if (index > parameters.length - 1)
      return match

    const position = parseInt(placeholder)

    if (Number.isInteger(position)) {
      if (position < 1)
        return ""
      return parameters[position - 1]
    }

    return parameters[index]
  })
}
